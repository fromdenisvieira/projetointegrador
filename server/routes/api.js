var userControl = require('../controllers/user.js');
var userValidator = require('../middlewares/validators/user.js');
var disciplinaControl = require('../controllers/disciplina.js');
var disciplinaValidator = require('../middlewares/validators/disciplina.js');
var ambienteControl = require('../controllers/ambiente.js');
var ambienteValidator = require('../middlewares/validators/ambiente.js');
var checkinControl = require('../controllers/checkin.js');
var checkinValidator = require('../middlewares/validators/checkin.js');
var msgControl = require('../controllers/mensagens.js');


module.exports = function(router,bodyParser){

/*
router.use(function(req,res,next){
	//verifica se o usuario esta logado ou se o usuario esta tentado se cadastrar
	if(req.isAuthenticated()||(req.path==='/users'&&req.method==='POST')){
		next();	
	}else{
		res.status(401);
		res.send("Acesso não autorizado");
	}
	
});
*/

router.route('/users')
	.head()
	.get(userControl.index)
	.post(bodyParser.json(),
		userValidator.create,
		userValidator.isUsernameUnico,
		userValidator.isEmailUnico,
		userValidator.isMatriculaUnico,
		userControl.create);

router.route('/users/:user')
	.get(/*userValidator.isUsuarioLogado,*/
		userControl.show)
	.put(bodyParser.json(),
		/*userValidator.isUsuarioLogado,*/
		userValidator.update,
		userValidator.isUsernameUnico,
		userValidator.isEmailUnico,
		userValidator.isMatriculaUnico,
		userControl.update);

router.route('/users/:user/disciplinas')
	.get(/*userValidator.isUsuarioLogado,*/
		userControl.indexDisciplinas)
	.post(bodyParser.json(),
		userValidator.createDisciplinas,
		/*userValidator.isUsuarioLogado,*/
		disciplinaValidator.existe,
		userControl.createDisciplinas)
	.delete(bodyParser.json(),
			userValidator.deleteDisciplinas,
			disciplinaValidator.existe,
			userControl.deleteDisciplinas);

router.route('/disciplinas')
	.get(disciplinaControl.index)
	.post(bodyParser.json(),
		disciplinaValidator.create,
		disciplinaValidator.isNomeDisciplinaUnico,
		disciplinaValidator.isSiglaDisciplinaUnico,
		disciplinaControl.create);

router.route('/disciplinas/:sigla')
	.get(disciplinaControl.show)
	.put(bodyParser.json(),
		disciplinaValidator.update,
		disciplinaValidator.isNomeDisciplinaUnico,
		disciplinaValidator.isSiglaDisciplinaUnico,
		disciplinaControl.update);

router.route('/disciplinas/:sigla/usuarios')
	.get(disciplinaControl.indexUsers);

router.route('/ambientes')
	.get(ambienteControl.index)
	.post(bodyParser.json(),
		ambienteValidator.create,
		ambienteValidator.isNomeAmbienteUnico,
		ambienteControl.create);

router.route('/ambientes/:nome')
	.get(ambienteControl.show)
	.put(bodyParser.json(),
		ambienteValidator.update,
		ambienteValidator.isNomeAmbienteUnico,
		ambienteControl.update);

router.route('/checkins')
	.get(checkinControl.index)
	.post(bodyParser.json(),
		checkinValidator.create,
		ambienteValidator.existe,
		disciplinaValidator.existe,
		userValidator.existe,
		checkinControl.create)
	.delete(bodyParser.json(),
			checkinControl.delete);

router.route('/mensagens')
	.get(msgControl.index);

}