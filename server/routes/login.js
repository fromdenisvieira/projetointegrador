

module.exports = function(app,passport,bodyParser){
	app.post('/login',
		bodyParser.json(),
		passport.authenticate('local'),
			function(req, res){
				if(req.user){
					res.json({username:req.user.username});
				}else{
					res.status(400);
					res.send('username ou senha errados');
				}
		});

	app.get('/logout',function(req,res){
		req.logout();
        res.send('logout feito');
	});
};