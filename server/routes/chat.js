var User = require('../models/user.js');
var Disciplina = require('../models/disciplinas.js');
var Mensagem = require('../models/chat/mensagens.js');

module.exports = function(chat){
chat.on('connection', function (socket) {
console.log('conectou');
	//Espera um username e verifica se já existe ou cria um novo usuario
	socket.on('connect user',function(username,callback){
		User.findOne({username:username},function(err,user){
			if(user){
					
					//grava o username no socket e altera o status do usuario para conectado
					socket.username = username;
					user.status = true;
					user.save(function(err,u){
						if(err){
							console.log(err);
						}
					});

					user.populate({path:'disciplinas',select:'sigla'},function(err,u){
						var disciplinas = u.disciplinas;
						var tam = disciplinas.length;
						var i = 0;
						for(i = 0; i < tam; i++){
							var sigla = disciplinas[i].sigla;
							socket.join(sigla);
							socket.broadcast.to(sigla).emit('user joined room',user.username,sigla);
						}
						callback({success:true,msg:'Your username is'+username,disciplinas:disciplinas});	
					});
			}else{
				callback({success:false,msg:'O usuário não existe',rooms:[]});
			}
		});
	});

	socket.on('has username',function(cb){
		if(socket.username){
			cb(true);
		}else{
			cb(false);
		}
	});


	//Ao se desconectar do servidor altera o estado do usuario para desconectado
	socket.on('disconnect',function(){
		console.log('disconnect');
		User.findOne({username:socket.username},function(err,user){
			
			console.log(err);
			if(user){
				user.status = false;
				user.save(function(err,u){
					console.log(u);
					if(err){
						console.log(err);
					}
					
				});

				user.populate({path:'disciplinas',select:'sigla'},function(err,u){
					var disciplinas = u.disciplinas;
					var tam = disciplinas.length;
					var i = 0;

					for(i=0; i<tam ; i++){
						chat.to(disciplinas[i].sigla).emit('disconnect from room',user.username,disciplinas[i].sigla);
					}
				});
			}
		});
	});

	

	

	socket.on('messaging',function(msg,disciplina,callback){
		if(socket.username){
			Disciplina.findOne({sigla:disciplina},function(err,disc){
				if(!disc){
					callback({success:false,msg:'Disciplina não existe'});
				}else{
					var mensagem = new Mensagem({text:msg,from:socket.username,disciplina:disciplina});
					mensagem.save(function(err,m){
						if(err){
							callback({success:false,msg:'nao foi possivel enviar a mensagem'})
						}else{
							socket.broadcast.to(disciplina).emit('messaging',msg,socket.username,disciplina);
							callback({success:true,msg:'Mensagem enviada!'});		
						}
					});
					
				}
			});
		}else{
			callback({success:false,msg:"Nenhum username vinculado"});
		}
	});

	
	/*socket.on('request offline data',function(disciplina_id,callback){
		User.findOne({username:socket.username},function(err,user){
			if(err) console.log(err);
			if(user){
				MsgToBeSent.find({user:user._id,disciplina:disciplina_id}).sort('time_sent')
				.populate({path:'disciplina from',select:'username name'})
				.exec(function(err,msgs){
						console.log(msgs);
						callback({success:true,msgs:msgs});	
										
				});
					
			}	
		});
	});

	socket.on('receive offline data',function(disciplina_id,user_id,callback){
		MsgToBeSent.remove({user:user_id,disciplina:disciplina_id},function(err){
			if(err){
				console.log(err);
				callback({success:false});
			}else{
				callback({success:true});
			}
		});
	});*/

		



	
});
}