var mongoose = require('mongoose');
var Schema = mongoose.Schema

var ambienteSchema = new Schema({
	nome: {type:String,required:true,unique:true},
	descricao:{type:String,required:true},
	bloco:{type:String ,required:true}

});


ambienteSchema.statics.findByNome = function(nome,cb){
	this.findOne({nome:nome},function(err,ambiente){
		if(err) console.log(err);
		cb(ambiente);
	});
};

ambienteSchema.statics.isUnico = function(data,cb){
	this.findOne(data,function(err,u){
		if(err) console.log(err);

		if(u){
			cb(false);
		}else{
			cb(true);
		}
	});
};

ambienteSchema.statics.existe = function(data,cb){
	this.findOne(data,function(err,u){
		if(err) console.log(err);

		if(u){
			cb(true);
		}else{
			cb(false);
		}
	});
}

module.exports = mongoose.model('ambiente',ambienteSchema);