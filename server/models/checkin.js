var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var checkinSchema = new Schema({
	time:{type:Date,default:Date.now},
	ambiente:{type:String,ref:'ambiente',required:true},
	disciplina:{type:String,ref:'disciplina',required:true},
	checkin_by:{type:String,ref:'user',required:true},
	aceito:{type:[{type:String,ref:'user'}],required:false,default:[]},
	nao_aceito:{type:[{type:String,ref:'user'}],required:false,default:[]}

});

checkinSchema.virtual('aceito.qtd').get(function(){
	return this.aceito.length;
});

checkinSchema.virtual('nao_aceito.qtd').get(function(){
	return this.nao_aceito.length;
});

checkinSchema.set('toJSON', { virtuals: true });

checkinSchema.statics.checkinsByDisciplinas = function(disciplina,cb){
	this.find({sigla:disciplina},function(err,checkins){
		if(err) console.log(err);
		cb(checkins);
	});
};

checkinSchema.statics.deletarCheckin = function(id_checkin,cb){

	this.remove({_id:id_checkin},function(err){
		if(!err){
			cb(true);
		}else{
			cb(false);
		}
	});

};

module.exports = mongoose.model('checkin',checkinSchema);

