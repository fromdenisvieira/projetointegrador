var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MensagensSchema = Schema({
	text : {type: String,required:true},
	from: {type:String,ref:'user.username',required:true},
	disciplina: {type:String,ref:'disciplina.sigla',required:true},
	time_sent : {type: Date, default: Date.now}

});

MensagensSchema.statics.ordenarPorTime = function(query,cb){
	this.find(query)
		.sort('time_sent')
		.exec(function(err,msgs){
			if(err){
				cb(false);
			}else{
				cb(msgs);
			}			
		});
}

module.exports = mongoose.model('mensagens',MensagensSchema);