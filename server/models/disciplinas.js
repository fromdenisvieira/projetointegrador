var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var disciplinaSchema = new Schema({
	nome: {type:String,required:true,unique:true},
	sigla: {type:String,required:true,unique:true},
	periodo: {type:String ,required:true},
	horario:{
		type:[
				{
					dia_da_semana:{type:Number,min:1,max:7,required:true},
					hora:{type:Number,min:0,max:24,required:true},
					minuto:{type:Number,min:0,max:60,required:true}
				}
			]
		},
	usuarios_cadastrados:{type:[{type:Schema.Types.ObjectId,ref:'user'}],default:[]}
});

disciplinaSchema.statics.findBySigla = function(sigla,cb){
	this.findOne({sigla:sigla},function(err,disc){
		if(err) console.log(err);
		cb(disc);
	});
};

disciplinaSchema.statics.isUnico = function(data,cb){
	this.findOne(data,function(err,d){
		if(err) console.log(err);

		if(d){
			cb(false);
		}else{
			cb(true);
		}
	});
}

disciplinaSchema.statics.existe = function(data,cb){
	this.findOne(data,function(err,d){
		if(err) console.log(err);

		if(d){
			cb(true);
		}else{
			cb(false);
		}
	});
};



disciplinaSchema.statics.formataHorario = function(horarios){
var h = [];
	for(var i = 0 ; i < horarios.length; i++){
		var horario = horarios[i].split('/');
		var dia_da_semana = horario[0];
		var hora = horario[1];
		var minuto = horario[2];
		h.push({dia_da_semana:dia_da_semana,hora:hora,minuto:minuto});
	}
	return h;

};

disciplinaSchema.statics.usuariosCadastrados = function(sigla,cb){

	this.findOne({sigla:sigla})
	.populate({
			path:'usuarios_cadastrados',
			select:'nome username email -_id'
	})
	.exec(function(err,disc){
		if(err){
			console.log(err);
			cb(false);
		}else{
			cb(disc.usuarios_cadastrados);
		}
	});

};

module.exports = mongoose.model('disciplina',disciplinaSchema);
