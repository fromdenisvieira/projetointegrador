var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

var Schema = mongoose.Schema;

var Disciplinas = require('../models/disciplinas.js');

var userSchema = Schema({
	nome:{type:String,required:true},
	username : {type: String, unique: true, required: true},
	email:{type: String,unique:true,required:true},
	senha:{type:String,required:true,set:function(senha){
		return bcrypt.hashSync(senha, bcrypt.genSaltSync(8), null);
	}},
	status : {type: Boolean, required: true,default:false},
	disciplinas:{type:[{type:Schema.Types.ObjectId,ref:'disciplina'}],required:false,default:[]}

});

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.senha);
};

userSchema.methods.getDisciplinas = function(cb){
	this.populate({path:'disciplinas'},function(err,user){
		if(err) console.log(err);

		cb(user.disciplinas);
	});
};

userSchema.statics.findByUsername = function(username,cb){
	this.findOne({username:username},function(err,u){
		if (err) console.log(err);

		cb(u);
	});
};

userSchema.statics.isUnico = function(data,cb){
	this.findOne(data,function(err,u){
		if(err) console.log(err);

		if(u){
			cb(false);
		}else{
			cb(true);
		}
	});
}

userSchema.statics.existe = function(data,cb){
	this.findOne(data,function(err,u){
		if(err) console.log(err);

		if(u){
			cb(true);
		}else{
			cb(false);
		}
	});
}

userSchema.statics.addDisciplina = function(usuario,sigla,cb){
	Disciplinas.findOne({sigla:sigla},function(err,disciplina){
		if(err) console.log(err);

		if(disciplina){
			
			if(usuario.disciplinas.indexOf(disciplina._id) === -1){
				usuario.disciplinas.push(disciplina._id);
				usuario.save(function(err){
					if(err){
						console.log(err);
						cb(false,'Não foi possivel cadastrar a disciplina');
					}else{
						disciplina.usuarios_cadastrados.push(usuario._id);
						disciplina.save(function(err){
							if(err) console.log(err);
							cb(true,'Disciplina cadastrada com sucesso');
						});
					}
				});
			}else{
				cb(false,'disciplina já cadastrada');
			}
		}else{
			cb(false,'disciplina não existe');
		}
	});
};


userSchema.statics.deleteDisciplina = function(usuario,sigla,cb){
	Disciplinas.findOne({sigla:sigla},function(err,disciplina){
		if(err) console.log(err);

		if(disciplina){
			var indice = usuario.disciplinas.indexOf(disciplina._id);
			if(indice > -1){
				usuario.disciplinas.splice(indice,1);
				usuario.save(function(err){
					if(err){
						console.log(err);
						cb(false,'Não foi possivel deletar a disciplina');
					}else{
						var indice = disciplina.usuarios_cadastrados.indexOf(usuario._id);
						disciplina.usuarios_cadastrados.splice(indice,1);
						disciplina.save(function(err){
							if(err) console.log(err);
							cb(true);
						});
					}
				});
			}else{
				cb(false,'disciplina não esta cadastrada');
			}
		}else{
			cb(false,'disciplina não existe');
		}
	});
};

module.exports = mongoose.model('user',userSchema);



















