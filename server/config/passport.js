var LocalStrategy   = require('passport-local').Strategy;

var User = require('../models/user.js');


module.exports = function(passport){

	passport.serializeUser(function(user, done) {
    done(null, user.id);
});

    // used to deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

passport.use(new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'senha',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, username, senha, done) { // callback with email and password from our form
        console.log(username);
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ 'username' :  username }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false); // req.flash is the way to set flashdata using connect-flash

            // if the user is found but the password is wrong
            if (!user.validPassword(senha))
                return done(null, false); // create the loginMessage and save it to session as flashdata

            // all is well, return successful user
            return done(null, user);
        });

    }));



};