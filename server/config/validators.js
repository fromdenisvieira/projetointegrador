var User = require('../models/user.js');
var Disciplina = require('../models/disciplinas.js');
var Ambiente = require('../models/user.js');

module.exports = {

	

	isHorarioCorreto : function(horarios){
		if(!Array.isArray(horarios)){
			return false;
		}else{
			for(var i = 0 ; i < horarios.length ;i++){
				var horario = horarios[i].split('/');
				var dia_da_semana = parseInt(horario[0]);
				var hora = parseInt(horario[1]);
				var minuto = parseInt(horario[2]);

				if(!(dia_da_semana>=1&&dia_da_semana<=7&&
					hora>=0&&hora<=24&&
					minuto>=0&&minuto<=60)){
					return false
				}

			}

			return true;
		}
	}



};