global.rootRequire = function(name) {
    return require(__dirname + '/' + name);
}
///Connect to the database
var mongoose = require('mongoose');
var configDB = require('./config/database.js');
mongoose.connect(configDB.url_mongolab);


//Stars the server for the api that listens on the 3000 port
var apiApp = require('express')();
apiApp.set('port', (process.env.PORT || 5000));
var bodyParser = require('body-parser');
apiApp.use(bodyParser.urlencoded({ extended: true }));
//loads the validation module
var expressValidator = require('express-validator');
var customValidators = require('./config/validators.js');
apiApp.use(expressValidator({customValidators:customValidators}));
//loads the session module
var session = require('express-session');
apiApp.use(session({ secret: 'teste' })); // session secret
//loads the passport module and configuration
var passport = require('passport');
require('./config/passport.js')(passport);
apiApp.use(passport.initialize());
apiApp.use(passport.session()); // persistent login sessions


//accepts request from other servers
apiApp.use(function (req, res, next) {
	console.log("oi");
    res.header('Access-Control-Allow-Origin', '*');//http://localhost:4000
    res.header('Access-Control-Allow-Methods', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Content-Type','application/json');
    next();
});



//load the module with the routes for the api
var apiRouter = require('express').Router();
require('./routes/api.js')(apiRouter,bodyParser);
apiApp.use('/api',apiRouter);
//load the route for login
require('./routes/login.js')(apiApp,passport,bodyParser);


var server2 = apiApp.listen(apiApp.get('port'),	function(){
	console.log('Node app is running on port',apiApp.get('port'));
})






