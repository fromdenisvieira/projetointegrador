

exports.create = function(req,res,next){
	req.checkBody('ambiente','obrigatorio informar o ambiente').notEmpty();
	req.checkBody('sigla','obrigatorio informar a sigla da disciplina').notEmpty();
	req.checkBody('username','obrigatorio informar o responsavel pelo checkin').notEmpty();

	req.erros = req.validationErrors();
	next();
};