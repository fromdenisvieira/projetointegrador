var Disciplina = rootRequire('/models/disciplinas.js');

exports.create = function(req,res,next){
	req.checkBody('nome','nome da disciplina obrigatorio').notEmpty();
	req.checkBody('sigla','sigla da disciplina obrigatoria').notEmpty();
	req.checkBody('periodo','informe um periodo de 1 a 8').isInt({min:1,max:8});
	req.checkBody('horario','informe o horario como um array de string no formato d/h/m').isHorarioCorreto();

	req.erros= req.validationErrors();
	next();

};

exports.update = function(req,res,next){
	req.checkBody('nome','nome da disciplina obrigatorio').optional().notEmpty();
	req.checkBody('sigla','sigla da disciplina obrigatoria').optional().notEmpty();
	req.checkBody('periodo','informe um periodo de 1 a 8').optional().isInt({min:1,max:8});
	req.checkBody('horario','informe o horario como um array de string no formato d/h/m').optional().isHorarioCorreto();

	req.erros= req.validationErrors();
	next();

};



function isUnico(field_name,field_data,req,next){
	var data;
	switch(field_name){
		case 'sigla':
			data =  {sigla:field_data};
			break;
		case 'nome':
			data = {nome:field_data};
			break;
		default:
			next();
	}
	
	Disciplina.isUnico(data,function(unico){
		if(!unico){
			var erro = {param:field_name,msg:field_name+" "+field_data+" ja existe",value:field_data}
				if(req.erros){
					req.erros.push(erro);	
				}else{
					req.erros = [erro];
				}
		}
		next();	
		
		
	});


};

exports.isNomeDisciplinaUnico = function(req,res,next){
	var nome = req.body.nome;
	if(nome){
		isUnico('nome',nome,req,next);	
	}else{
		next();
	}
};

exports.isSiglaDisciplinaUnico = function(req,res,next){
	var sigla = req.body.sigla;
	if(sigla){
		isUnico('sigla',sigla,req,next);	
	}else{
		next();
	}
};

exports.existe = function(req,res,next){
	var sigla_body = req.body.sigla;
	var sigla_param = req.params.sigla;
	var sigla = sigla_param ? sigla_param : sigla_body ;
	if(!sigla){
		next();
	}else{
		Disciplina.existe({sigla:sigla},function(existe){
			if(!existe){
				var erro = {param:"sigla",msg:"sigla "+sigla+" não existe",value:sigla};
				if(req.erros){
					req.erros.push(erro);
				}else{
					req.erros = [erro];	
				}			
			}
			next();		
		});
	}
};