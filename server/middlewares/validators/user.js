var User = rootRequire('/models/user.js');

exports.create = function(req,res,next){
	req.checkBody('nome','nome obrigatorio').notEmpty();
	req.checkBody('username','username obrigatorio').notEmpty();
	req.checkBody('email','email invalido').notEmpty().isEmail();
	req.checkBody('senha','senha obrigatoria').notEmpty();
	req.erros= req.validationErrors();
	next();
	
};

exports.update = function(req,res,next){
	req.checkBody('nome','nome obrigatorio').optional().notEmpty();
	req.checkBody('username','username obrigatorio').optional().notEmpty();
	req.checkBody('email','email invalido').optional().notEmpty().isEmail();
	req.checkBody('senha','senha obrigatoria').optional().notEmpty();
	req.erros= req.validationErrors();
	next();
};

exports.createDisciplinas = function(req,res,next){
	req.checkBody('sigla','sigla da disciplina obrigatoria').notEmpty();
	var erro = req.validationErrors();
	if(erro){
		res.json(400,erro);
	}else{
		next();
	}
}

exports.deleteDisciplinas = function(req,res,next){
	req.checkBody('sigla','sigla da disciplina obrigatoria').notEmpty();
	var erro = req.validationErrors();
	if(erro){
		res.json(400,erro);
	}else{
		next();
	}
}

exports.isUsuarioLogado = function(req,res,next){
	var user_informado = req.params.user;
	var user_logado = req.user ? req.user.username: false;
	if( user_logado !== user_informado){
		res.send(403,'Voce não tem acesso a dados de outros usuarios');
	}else{
		next();
	}
};


exports.existe = function(req,res,next){
	var username = req.body.username;
	if(!username){
		next();
	}else{
		User.existe({username:username},function(existe){
			if(!existe){
				var erro = {param:"username",msg:"username "+username+" não existe"}
				if(req.erros){
					req.erros.push(erro);	
				}else{
					req.erros = [erro];
				}
				
			}
			next();
		});
	}

	
};




function isUnico(field_name,field_data,req,next){
	var data;
	switch(field_name){
		case 'username':
			data =  {username:field_data};
			break;
		case 'email':
			data = {email:field_data};
			break;
		case 'matricula':
			data = {matricula:field_data};
			break;
		default:
			next();
	}
	
	User.isUnico(data,function(unico){
		if(!unico){
			var erro = {param:field_name,msg:field_name+" "+field_data+" ja existe",value:field_data}
				if(req.erros){
					req.erros.push(erro);	
				}else{
					req.erros = [erro];
				}
		}
		
		next();	
		
		
	});


};

exports.isUsernameUnico = function(req,res,next){
	var username = req.body.username;
	if(username){
		isUnico('username',username,req,next);	
	}else{
		next();
	}
};

exports.isEmailUnico = function(req,res,next){
	var email = req.body.email;
	if(email){
		isUnico('email',email,req,next);		
	}else{
		next();
	}
	
};

exports.isMatriculaUnico = function(req,res,next){
	var matricula = req.body.matricula;
	if(matricula){
		isUnico('matricula',matricula,req,next);		
	}else{
		next();
	}
	
};



