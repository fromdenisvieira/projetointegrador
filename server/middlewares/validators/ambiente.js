var Ambiente = rootRequire('/models/ambientes.js');


exports.create = function(req,res,next){
	req.checkBody('nome','nome obrigatorio').notEmpty();
	req.checkBody('descricao','descricao obrigatorio').notEmpty();
	req.checkBody('bloco','bloco obrigatorio').notEmpty();

	req.erros = req.validationErrors();
	next();
};

exports.update = function(req,res,next){
	req.checkBody('nome','nome obrigatorio').optional().notEmpty();
	req.checkBody('descricao','descricao obrigatorio').optional().notEmpty();
	req.checkBody('bloco','bloco obrigatorio').optional().notEmpty();

	req.erros = req.validationErrors();
	next();
};

exports.existe = function(req,res,next){
	var nomeAmbiente = req.body.ambiente;
	if(!nomeAmbiente){
		next();
	}else{
		Ambiente.existe({nome:nomeAmbiente},function(existe){
			if(!existe){
				var erro = {param:"ambiente",msg:"ambiente "+nomeAmbiente+" não existe",value:nomeAmbiente};
				if(req.erros){
					req.erros.push(erro);
				}else{
					req.erros = [erro];	
				}			
			}
			next();		
			
		
		});
	}
}

exports.isNomeAmbienteUnico = function(req,res,next){
	var nome = req.body.nome;
	if(!nome){
		next();
	}else{
	
		Ambiente.isUnico({nome:nome},function(unico){
			if(!unico){
				var erro = {param:"nome",msg:"ambiente "+nome+" ja existe",value:nome};
					if(req.erros){
						req.erros.push(erro);	
					}else{
						req.erros = [erro];
					}
			}
			next();	
			
			
		});
	}


};