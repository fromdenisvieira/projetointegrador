var User = require('../models/user.js');


exports.index = function(req,res){
	User.find(req.query,function(err,users){
		
		res.json(users);	
	});
};

exports.create = function(req,res){
	if(req.erros){
		res.status(400);
		res.json(req.erros);
	}else{
		var user_data = req.body;
		var user  = new User(user_data);

		user.save(function(err,u){
			if(err) console.log(err);
			if(u){
				res.json({msg:"Usuario criado com sucesso!"});
			}else{
				res.status(500);
				res.send(err);
			}
		});
	}
};

exports.show = function(req,res){
	if(req.erros){
		res.status(400);
		res.json(req.erros);
	}else{
		User.findByUsername(req.params.user,function(usuario){
			if(usuario){
				res.json({success:true,usuario:usuario});
			}else{
				res.status(400);
				res.send('Usuario não encontrado');
			}
		});
	}
}

exports.update = function(req,res){
	User.findByUsername(req.params.user,function(usuario){
		if(usuario){
			for(var k in req.body){
				usuario[k] = req.body[k];
			}
			
			usuario.save(function(err){
				if(err){
					res.status(500);
					res.send(err);
				}else{
					res.json({msg:"Usuario alterado com sucesso!"});
				}
			});

		}else{
			res.status(400);
			res.send('O username informado não existe');
		}
	});

};

exports.indexDisciplinas = function(req,res){
	User.findByUsername(req.params.user,function(usuario){
		if(usuario){
			usuario.getDisciplinas(function(disciplinas){
				res.json(disciplinas);
			});
		}else{
			res.send(400,'o username não existe');
			
		}
	});

};

exports.createDisciplinas = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		User.findByUsername(req.params.user,function(usuario){
			User.addDisciplina(usuario,req.body.sigla,function(success,msg){
				res.json({msg:msg});
			});
		});
	}

};

exports.deleteDisciplinas = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		User.findByUsername(req.params.user,function(usuario){
			User.deleteDisciplina(usuario,req.body.sigla,function(success,msg){
				if(success){
					res.send('disciplina deletada com sucesso');
				}else{
					res.status(400).send(msg);
				}
				
			});
		});
	}

};





