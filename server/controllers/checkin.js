var Checkin = require('../models/checkin.js');

exports.index = function(req,res){
	Checkin.find(req.query,function(err,checkin){	
		
		res.json(checkin);	
	});
	

};

exports.create = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		var checkin_data = req.body;
		checkin_data.checkin_by = req.body.username;
		checkin_data.disciplina = checkin_data.sigla;
		var checkin  = new Checkin(checkin_data);

		checkin.save(function(err,c){
			if(err) console.log(err);
			if(c){
				res.send("checkin feito com sucesso!");
			}else{
				res.status(500).send("Não foi possivel fazer o checkin!");
			}
		});
	}
};

exports.delete = function(req,res){
	Checkin.deletarCheckin(req.body.id,function(deletado){
		if(deletado){
			res.send('deletado com sucesso');
		}else{
			res.status(500).send('não foi possivel deletar');
		}
	});
};

