var Mensagem = require('../models/chat/mensagens.js');

exports.index = function(req,res){
	Mensagem.ordenarPorTime(req.query,function(msgs){
		if(msgs){
			res.json(msgs);
		}else{
			res.status(500).send('Ocorreu um erro');
		}
	});
};