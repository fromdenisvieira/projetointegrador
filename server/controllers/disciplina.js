var Disciplinas = require('../models/disciplinas.js');

exports.index = function(req,res){
	Disciplinas.find(req.query)
		.populate({path:'usuarios_cadastrados',select:'username -_id'})
		.exec(function(err,d){
				if(err){
					res.status(500).send(err);		
				}else{
					res.json(d);
				}
			});
};

exports.create = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		var disc_data = req.body;
		var horario = disc_data.horario;
		
		disc_data.horario = Disciplinas.formataHorario(horario);
		var disciplina  = new Disciplinas(disc_data);

		disciplina.save(function(err,d){
			if(err) console.log(err);
			if(d){
				res.send("disciplina criada com sucesso!");
			}else{
				res.status(500).send("não foi possivel criar a disciplina!");
			}
		});
	}
};

exports.show = function(req,res){
	Disciplinas.findBySigla(req.params.sigla,function(disciplina){
			if(disciplina){
				res.json({disciplina:disciplina});
			}else{
				res.status(400).send("Disciplina não encontrada!");
			}
	});
	
};

exports.update = function(req,res){
	Disciplinas.findBySigla(req.params.sigla,function(disciplina){
		if(disciplina){
			for(var k in req.body){
				if(k==='horario'){
					req.body[k] = Disciplinas.formataHorario(req.body[k]);
				}
				disciplina[k] = req.body[k];
			}
			
			disciplina.save(function(err){
				if(err){
					res.send(500,"não foi possivel alterar a disciplina!");
				}else{
					res.send("disciplina alterada com sucesso!");
				}
			});

		}else{
			res.send(400,"a disciplina informada não existe");
		}
	});

};

exports.indexUsers = function(req,res){
	Disciplinas.usuariosCadastrados(req.params.sigla,function(users){
		if(!users){
			res.status(500).send('aconteceu um erro');
		}else{
			res.json(users);
		}
	});

};

	




