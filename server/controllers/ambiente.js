var Ambiente = require('../models/ambientes.js');


exports.index = function(req,res){
	Ambiente.find(req.query,function(err,ambientes){
		
		res.json(ambientes);	
	});
};

exports.create = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		var amb_data = req.body;
		var ambiente  = new Ambiente(amb_data);

		ambiente.save(function(err,a){
			if(err) console.log(err);
			if(a){
				res.send("Ambiente criado com sucesso!");
			}else{
				res.status(500).send("Não foi possivel criar o ambiente!");
			}
		});

	}
};

exports.show = function(req,res){
	Ambiente.findByNome(req.params.nome,function(ambiente){
		if(ambiente){
			res.json({ambiente:ambiente});
		}else{
			res.status(400).send("ambiente não encontrado!");
		}
	});
}

exports.update = function(req,res){
	if(req.erros){
		res.status(400).json(req.erros);
	}else{
		Ambiente.findByNome(req.params.nome,function(ambiente){
			if(ambiente){
				for(var k in req.body){
					ambiente[k] = req.body[k];
				}
				
				ambiente.save(function(err){
					if(err){
						res.status(500).send("Não foi possivel alterar o ambiente!");
					}else{
						res.send("Ambiente alterado com sucesso!");
					}
				});

			}else{
				res.status(400).send("O ambiente informado não existe");
			}
		});
	}

};