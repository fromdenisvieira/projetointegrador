## INSTRUÇÕES PARA O SERVIDOR


1. Instale o MONGODB através do [Site Oficial](http://www.mongodb.org/downloads).

2. Instale também o NODE.JS através do [Site Oficial](https://nodejs.org/download/).

3. Entre na pasta do server através do terminal .
```
cd ./server
```
4. Instale as dependências através do comando.
```
npm install
```
5. Inicie o serviço do mongo no terminal.
```console
mongod
```
6. Execute o app.js com o node.
```console
cd ./server/
node app.js
```
7. Se a resposta ao comando ficar em branco, não, não está demorando, está ok e a api estará em : 
```
API LOCAL : localhost:3000/api  
CHAT LOCAL: localhost:80/chat

API HEROKU : https://projetoifal.herokuapp/api  
CHAT HEROKU : https://projetoifal.herokuapp/chat
ARQUIVO JS SOCKET.CLIENT : https://projetoifal.herokuapp/socket.io/socket.io.js
```
8. Para testes mais práticos instale o [postman](https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm) extensão do chrome, e verifique as requições com as urls mais abaixo . 

9. Ou já teste no cliente seguindo as [Instruções para o Client](../client/InstrucoesClient.md).

### TABLES

| Usuario        | Disciplina | Ambiente  | Checkin
|:-------------:|:-------------:|:-----:|:-----:
|username| nome | nome | time
|email|sigla|descricao| ambiente
|senha|periodo|bloco| usuario
|matricula|horario || concorda
|tipo| usuarios || nao_concorda
| disciplinas | ||




### URLS DA API

```
localhost/login
post - recebe um json com username e senha e retorna um json com o sucesso do login
```

```
localhost/api/(url-abaixo)
```

```
head: /users
-retorna http success 200 se o usuario estiver logado e http error 401 caso não esteja
get: /users 
- json com todos os usuarios
post: /users 
- cria o usuario e retorna um json com {success,msg}
```

```
get: /users/{username do usuario} 
- retorna json com os dados do usuario
put: /users/{username do usuario} 
- altera o usuario e retorna um json {success,msg}
```

```
get: /users/{username do usuario}/disciplinas 
- retorna as disciplinas que o usuario se cadastrou
post: /users/{username do usuario}/disciplinas 
- adiciona uma disciplina ao usuario(no post manda a sigla da disciplina)
delete: /users/{username do usuario}/disciplinas 
- remove uma disciplina do usuario(no delete manda a sigla da disciplina)
```

```
get: /disciplinas 
- json com todas as disciplinas
post: /disciplinas 
- cria a disciplina(o horario deve ser informado com um array de string no formato '{dia da semana(1-7)}/{hora}/{minuto}' e retorna json {success,msg}
```

```
get: /disciplinas/{sigla da disciplina} 
- retorna json com dados da disciplina
post: /disciplinas/{sigla da disciplina} 
- atualiza disciplina e retorna json {success,msg}
```

```
get: /ambiente 
- json com todos os ambientes
post: /ambiente 
- cria o ambiente e retorna json {success,msg}
```

```
get: /ambiente/{nome do ambiente} 
- retorna json com dados do ambiente
post: /ambiente/{nome do ambiente} 
- atualiza ambiente e retorna json {success,msg}
```

```
get: /checkins 
- json com todos os checkins
post: /checkns 
- cria o checkin e retorna json {success,msg}
delete: /checkins
- recebe um json com o id do checkin e deleta
```

### EVENTOS DO CHAT

##### O cliente vai emitir os eventos :

```
emit('connect user',username,calback); 
- associa o socket ao username e recebe uma callback com um json {sucess:boolean,msg:string,disciplinas:array}
```

```
emit('has username',calback); 
- verifica se o socket possui username e retorna true ou false
```

```
emit('messaging',msg,siglaDisciplina,callback); 
- envia mensagem para a uma sala e recebe callback com json{success,msg}
```

```
emit('request offline data',disciplina_id,callback); 
- requisita msgs mandadas para a sala enquanto usuario estava offline
```

```
emit('receive offline data',disciplina_id,user_id,callback) 
- confirma recebimento das msg da sala e recebe callback com json{success}
```

#####  O cliente irá receber os eventos :

```
on('disconnect from room',function(username){
	recebe o username do usuario q de desconectou
}); 
```

```
on('user joined room',function(username,disciplina){
	recebe o username do usuario q se conectou e a nome da sala
});
```

```
on('messaging',function(msg,user,disciplina){
	recebe a mensagem , o usuario que enviou e a sala pra qual foi enviada
});
```
















