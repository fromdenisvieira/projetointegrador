# Projeto Integrador I
## Sistemas de Informação - IFAL

### EQUIPE
* Denis Vieira
* Djanilson Alves
* João Victor

### [Instruções para o Servidor](./server/InstrucoesServer.md) | [Instruções para o Cliente](./client/InstrucoesClient.md)


### Fluxo de Trabalho

Estamos usando o git como controle de versão, então o fluxo de trabalho será basicamente o seguinte :
####Guias 
####[git - guia prático](http://rogerdudler.github.io/git-guide/index.pt_BR.html) | [comandos básicos do git](http://blog.gustavohenrique.net/2011/03/comandos-basicos-do-git/)

#### Configuração

Configurando informações sobre o autor dos commits:
```
git config --global user.name "Gustavo"
git config --global user.email "gustavo@gustavohenrique.net"
```
Clonando o repositório do projeto:
```
cd meuprojeto
git clone usuário@servidor:/caminho/para/o/repositório 
```
#### Workflow Básico

Trabalharemos com duas branchs a DEVELOP, onde adicionaremos as modificações diárias e a MASTER que será nossa branch de produção e ficará nossa aplicação final .

Basicamente após a configuração : 

1. Atualizaremos o nosso repositorio ( Fazer toda vez que for iniciar os trabalhos no projeto !)
```
git pull
```

2. Trabalhamos nas modificações que precisam ser feitas

3. Adicionamos os arquivos novos ou modificados no indice ( local ):
```
git add arquivo.txt
git add *.py
git add . (para add todos os arquivos)
git add -i (para modo interativo. 1-5 ou 1,2,3,4 e -3 para retirar)
git add --all (caso houver deletado algum arquivo)
```

4. Adicionamos os arquivos novos ou modificados no head ( projeto ):
```
git commit -m "msg, informação da alteração"
```

5. Adicionando arquivos novos ou modificados no repositorio online
```
git push origin develop
```

*OBS : Verifique em qual branch está executando esses comandos, aconselho usar o proprio git bash que da a indicação com o nome da branch na propria linha .
Fazer toda vez que for trabalhar no projeto !!