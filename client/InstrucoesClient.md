## INSTRUÇÕES PARA O CLIENT

1. Inicie o servidor seguindo as [Instruções para o Servidor](../server/InstrucoesServer.md).

2. Entre na pasta do client através do terminal .
```
cd ./client
```
3. Instale as dependências através do comando
```
npm install
```
>e
```
bower install
```
4. Para simplesmente utilizar o app, inicie o cliente atravês do comando :
```
node app.js
```
5. E pronto acesse aplicação pelo browser através do endereço :
```
localhost/3001
```

6. Para o desenvolvimento inicie através do NPM para algumas tasks importantes :
```
npm start
```

7. E pronto acesse aplicação pelo browser e entre no cliente em modo de desenvolvimento :
```
localhost/4000
```


### Guide for folders




  