var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var uglify       = require('gulp-uglify');
var rename = require('gulp-rename');
var sh = require('shelljs');

var paths = {
  sass: ['./www/scss/**/*.scss']
};

gulp.task('default', ['sass'/*,'scripts'*/,'express', 'livereload','watch']);

gulp.task("sass", function () {
    var sass = require("gulp-sass");

    return gulp.src("./www/scss/**/*.scss")
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: "extended"
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest("./www/css/"));
});

gulp.task('express', function() {
  var express = require('express');
  var http = require('http');
  var app = express();
  app.use(require('connect-livereload')({port: 4002}));
  app.use(express.static(__dirname+"/www"));
  // app.listen(4000);
  //     console.log("\n" +
  //       "===============================================\n" +
  //       " Application running at: http://localhost:4000 \n" +
  //       "===============================================\n"
  //   );

  app.server = http.createServer(app);
  app.server.listen(4000);
});

var tinylr;
gulp.task('livereload', function() {
  tinylr = require('tiny-lr')();
  tinylr.listen(4002);
});

function notifyLiveReload(event) {
  var fileName = require('path').relative(__dirname, event.path);

  tinylr.changed({
    body: {
      files: [fileName]
    }
  });
}
/*
gulp.task('scripts',function(){
  return gulp.src(paths.js)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./www/js/'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./www/js/'));
})
*/


gulp.task('watch', function() {
  gulp.watch(["./www/scss/**/*.scss"], ["sass"]);
  gulp.watch('./www/views/**/*.js').on('change', notifyLiveReload);
  gulp.watch('./www/views/**/*.html').on('change', notifyLiveReload);
  gulp.watch('./www/views/**/**/*.html').on('change', notifyLiveReload);
  gulp.watch('./www/views/**/**/*.js').on('change', notifyLiveReload);
});



