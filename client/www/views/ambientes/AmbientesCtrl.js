(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('AmbientesCtrl', ['$scope', 'AmbientesFactory', '$rootScope', function($scope, AmbientesFactory, $rootScope) {

		AmbientesFactory.listar().then(function(data){
			$scope.ambientes = data;			
			$scope.total = data.length;
		},
		function(){
			alert('erro');
		}); 	

		$scope.salvarAmbiente = function(){

	    	   AmbientesFactory.salvar($scope.ambiente).then(function(data){
	    	   		    
	    	   },
	    	   function(){
	    	   	 alert('erro');
	    	   });	    	   
	    };

	}]);

} ());