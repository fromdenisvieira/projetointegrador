(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('AmbientesFactory', ['$http', '$q','$location', function($http,$q, $location) {

		var listar = function(data){

			var retorno = $q.defer();

			$http.get('http://localhost:3000/api/ambientes').success(function(data) {

				retorno.resolve(data);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 

			return retorno.promise;

		}
		
		
        var salvar = function (ambiente) {

			     	
        	var ambienteToJson = function (){
                 return angular.toJson({
                	 "nome": ambiente.nome,
                     "descricao": ambiente.descricao,
                     "bloco": ambiente.bloco
                 });
             };

            console.log("AMBIENTE :: " +ambienteToJson());
             
            var retorno = $q.defer();  

            $http.post('http://localhost:3000/api/ambientes', ambienteToJson())
             .success(function(data) {
            	 $location.path('ambientes');
			})
			.error(function(data) {
				alert("Não foi possível Cadastrar o Ambiente");
			});
                          
            return retorno.promise;

        }
        
        var excluir = function (idDisciplina){

			var retorno = $q.defer();
			
			var sequencial = parseInt(idDisciplina);
			console.log(sequencial);
			
			if(window.confirm("Tem certeza que deseja excluir a disciplina de ID : " +sequencial)){


				$http.delete('http://localhost:3000/api/ambientes'+sequencial)
				.success(function() {

					retorno.resolve("Disciplina excluída com sucesso!!!!");
					$route.reload();
				})
				.error(function(data) {
					alert("Não foi possível cadastrar! Verifique sua conexão de internet!");
				});

			}

			return retorno.promise;

		}
    
        function atualizar (ambiente) {
        	
        	var retorno = $q.defer();
        	        	
        	 var ambienteToJson = function (){
                 return angular.toJson({
                	 "id": disciplina.id,
                     "nome": disciplina.nome,
                     "informacoes": disciplina.informacoes
                 });
             };

             console.log("EDITAR :: "+ambienteToJson());
             
             $http.put('http://localhost:3000/api/ambientes/'+ambiente.nome, ambienteToJson())
             .success(function(data) {
            	retorno.resolve("Disciplina atualizada com Sucesso!!!");
				
				$window.location.reload();
				
			})
			.error(function(data) {
				alert("Não foi possível atualizar! Verifique sua conexão de internet!");
			});
             
             return retorno.promise;
        	
		}

		return {
			
			listar: listar,
			salvar: salvar,
			excluir: excluir,
			atualizar: atualizar
		};

	}]);


}());