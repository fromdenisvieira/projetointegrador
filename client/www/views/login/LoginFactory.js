(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('LoginFactory', ['$http', '$location','$q','$timeout','localStorageService', function($http, $location,$q,$timeout,localStorageService) {

		 var authenticatedUser = null;

		function requestUser()
	        {
	            var deferred = $q.defer();

	            $http.get('http://projetoifal.herokuapp.com/api/users').success(function(user)
	            {
	                
	                    // Check if user is defined first
	                    if(user) {

	                        authenticatedUser = user;
	                    }

	                    deferred.resolve(authenticatedUser);
	              
	                
	            }).error(function(error)
	            {
	                deferred.reject(error);
	            });

	            return deferred.promise;
	        }

	    function getUser()
        {
            return authenticatedUser;
        }

        function exists()
        {
            return authenticatedUser != null;
        }

		function login(usuario) {

			var deferred = $q.defer();

			var usuarioToJson = function (){
				return angular.toJson({
					"username": usuario.username,
					"senha": usuario.senha
				});
			};
			console.log(usuarioToJson());

			$http.post('http://projetoifal.herokuapp.com/login', usuarioToJson())
			.success(function(result) {
				
				console.log("Usuario logado com sucesso ! username : "+result.username);
				
				localStorageService.set('username',result.username);
				/*	if(result.success)
                {
                    authenticatedUser = usuario.username;
                    deferred.resolve(usuario);
                    console.log('authenticatedUser = '+usuario);
                }
                else
                {
                    deferred.reject('Given credentials are incorrect');
                }
				
				*/
				$location.path('dashboard');
				deferred.resolve(result);
				
			})
			.error(function(error) {
				console.log("Não foi possível Conectar");
				$location.path('/');
				deferred.reject(error);
			});

			return deferred.promise;

		}

		function logout(usuario) {
			var deferred = $q.defer();

			$http.get('http://projetoifal.herokuapp.com/logout')
			.success(function(data) {
				
				//console.log("retorno do logout:" + data.success);
				//authenticatedUser = null;	
				var username = localStorageService.get('username');
				localStorageService.remove(username);
				$location.path('/');
				deferred.resolve(data);
			})
			.error(function(error) {
				console.log("Não foi possível efetuar o logout");
				deferred.reject(error);
			});

			return deferred.promise;
		}

		
		return {
			requestUser: requestUser,
			getUser: getUser,
			exists: exists,
			login: login,
			logout: logout
		}


	}])
	.service('AuthService',function AuthService(){

		 var ready = false, registeredListeners = [];

	    var callListeners = function()
	    {
	        for (var i = registeredListeners.length - 1; i >= 0; i--) {
	            registeredListeners[i]();
	        };
	    }

	    return {
	        isReady: function()
	        {
	            return ready;
	        },

	        makeReady: function()
	        {
	            ready = true;

	            callListeners();
	        },

	        registerListener: function(callback)
	        {
	            if(ready) callback();
	            else      registeredListeners.push(callback);
	        }
	    }	

	});


}());