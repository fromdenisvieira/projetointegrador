(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('LoginCtrl', ['$scope', 'LoginFactory', '$rootScope','$location', function($scope, LoginFactory, $rootScope,$location) {


		// Validacao Fake
	    $scope.falhou = false;
	    $scope.Padrao = {};
	    $scope.Padrao.username = 'admin';
	    $scope.Padrao.senha = '123';

	    $scope.usuario = {
		    username: '',
		    senha: ''
		};

	    $scope.login = function() {
			 $scope.falhou = false;
	    	LoginFactory.login($scope.usuario).then(function(){

			      //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			      //$scope.setCurrentUser(user);
		    }, function () {
		        $scope.falhou = true;
		    });
	    	 
		}


	  	$scope.doLogin = function (){
	    	// simular o Login no aplicativo
	    	if($scope.username === $scope.Padrao.username &&
	      	$scope.senha === $scope.Padrao.senha){
	      	// ir para o Dashboard
	     	$location.path('dashboard');
	    	}else {
	      	// TODO - Implementar o alerta nos padroes BootStrap
	      	$scope.falhou = true;
	    	}
	 	};



	}]);

} ());