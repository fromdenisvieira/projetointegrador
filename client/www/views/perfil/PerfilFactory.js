(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('PerfilFactory', ['$http', '$location','$q','localStorageService', function($http, $location,$q,localStorageService) {

		var listar = function(){

			var retorno = $q.defer();

			var username = localStorageService.get('username');

			$http.get('http://projetoifal.herokuapp.com/api/users/'+username).success(function(data) {

				retorno.resolve(data);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 

			return retorno.promise;

		}
		
		
        var salvar = function (disciplina) {

			

        }
        
        var excluir = function (idDisciplina){

			

		}
    
        

        var atualizar = function (usuario) {

        	var username = localStorageService.get('username');
        	// var oldUser = localStorageService.get('oldUser');
        	
        	var retorno = $q.defer();

        	// console.log(oldUser);
        	// console.log(newUser);

        	// var usuario = [];

        	// usuario.name.push(newUser.nome);
        	

        	// if(oldUser.nome != newUser.nome){
        	// 	usuario.nome = newUser.nome;
        	// }

        	// if(oldUser.username != newUser.username){
        	// 	var usuario.username = newUser.username;
        	// }

        	// if(oldUser.senha != newUser.senha){
        	// 	var usuario.senha = newUser.senha;
        	// }

        	// if(oldUser.email == newUser.email){
        	// 	var usuario.email = newUser.email;
        	// }
	    	
	    	var usuarioToJson = function (){

	    		return angular.toJson({					
				    "nome": usuario.nome,
			        "username": usuario.username,			        
			        "email": usuario.email
				    // "matricula": usuario.matricula,
				    // "tipo": usuario.tipo  

				});
			};

			console.log(usuarioToJson());

			$http.put('http://projetoifal.herokuapp.com/api/users/'+username, usuarioToJson())
			.success(function(result) {

				$location.path('/configuracoes');
				console.log("Usuario Modificado com Sucesso !");
				

				retorno.resolve(result);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;
        	
        	
		}


		return {
			
			listar: listar,
			salvar: salvar,
			excluir: excluir,
			atualizar: atualizar
		};

	}]);


}());