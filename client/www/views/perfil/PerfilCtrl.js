(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('PerfilCtrl', ['$scope', '$rootScope','$location','LoginFactory','PerfilFactory', function($scope, $rootScope,$location,LoginFactory,PerfilFactory) {


		PerfilFactory.listar().then(function(data){
			console.log(data.usuario);
			$scope.usuario = data.usuario;
			
		},
		function(){
			alert('erro');
		}); 

		$scope.atualizar = function(usuario) {
			 $scope.falhou = false;
	    	PerfilFactory.atualizar(usuario).then(function(){

			      //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			      //$scope.setCurrentUser(user);
		    }, function () {
		        $scope.falhou = true;
		    });
	    	 
		}


	}]);

} ());