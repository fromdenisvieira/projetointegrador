(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('RecuperarSenhaFactory', ['$http', '$location','$q', function($http, $location,$q) {

		var solicitar = function(email){

			var usuarioToJson = function (){

	    		return angular.toJson({					
				    
				    "email":usuario.email

				});
			};

			console.log(usuarioToJson());
			var retorno = $q.defer();

			$http.post('http://localhost:3000/api/recuperarsenha', usuarioToJson())
			.success(function(result) {

				$location.path('/');
				console.log("Usuario Cadastrado com Sucesso !")

				retorno.resolve(result);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;

		}


		
		return {
			
			solicitar: solicitar
		};

	}]);


}());