(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('RecuperarSenhaCtrl', ['$scope','RecuperarSenhaFactory', '$rootScope','$location', function($scope, RecuperarSenhaFactory, $rootScope,$location) {


		$scope.$back = function() { 
		    window.history.back();
		};

		$scope.recuperar = function(email) {
			 $scope.falhou = false;
	    	RecuperarSenhaFactory.solicitar(email).then(function(){

			      //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			      //$scope.setCurrentUser(user);
		    }, function () {
		        $scope.falhou = true;
		    });
	    	 
		}



	}]);

} ());