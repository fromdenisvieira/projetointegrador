(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('ChatFactory', ['$http', '$location','$q','localStorageService','$route','$routeParams', function($http, $location,$q,localStorageService,$route,$routeParams) {

		var listar = function(sequencial){

			var retorno = $q.defer();

			$http.get('http://projetoifal.herokuapp.com/api/mensagens?disciplina='+$route.current.params.sigla).success(function(data) {

				retorno.resolve(data);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 

			return retorno.promise;

		};


		
		
        var salvar = function (sigla) {


        };
        
        var excluir = function (siglaDisc){

        

		};
    
        function atualizar (disciplina) {
        	
        	
        	
		};

		return {
			
			listar: listar,
			salvar: salvar,
			excluir: excluir,
			atualizar: atualizar
		};

	}]);


}());