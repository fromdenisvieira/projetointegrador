(function() {

	'use strict';

	

	angular.module('projetointegrador')
	.factory('socket', function (socketFactory) {
		// var socket = io({
		// 		  transports: [
		// 		    'websocket', 
		// 		    'flashsocket', 
		// 		    'htmlfile', 
		// 		    'xhr-polling', 
		// 		    'jsonp-polling', 
		// 		    'polling'
		// 		  ]
		// 		});

		  var socket = io.connect('https://projetoifal.herokuapp.com/chat');

		  var mySocket = socketFactory({
		    ioSocket: socket
		  });

		  return mySocket;
	  })
	.value('messageFormatter', function(date, nick, message) {
    return date.toLocaleTimeString() + ' - ' + 
           nick + ' - ' + 
           message + '\n';
    
  	})
  	.value('nickName', 'anonymous')
	.controller('ChatCtrl', ['$scope','$route','$routeParams','socket','localStorageService','ChatFactory', function($scope,$route,$routeParams,socket,localStorageService,ChatFactory) {

		
		ChatFactory.listar().then(function(data){
			$scope.mensagens = data;			
		},
		function(){
			alert('erro');
		}); 

		$scope.siglaDisciplina = $route.current.params.sigla;

		var username = localStorageService.get('username');
		
				

		// //- associa o socket ao username e recebe uma callback com um json {sucess:boolean,msg:string,disciplinas:array}
		// socket.emit('connect user',username,function(data){
		// 	console.log(data);
		// });


		//- envia mensagem para a uma sala e recebe callback com json{success,msg}
		$scope.sendMsg = function(mensagem){
			socket.emit('messaging',mensagem,$scope.siglaDisciplina,function(data){
				console.log(data);
				$scope.mensagens.push({
					"text":mensagem,
					"from":username,
					"disciplina":$scope.siglaDisciplina
				});
				$scope.$digest();
				$scope.mensagem = '';
			}); 
			//$scope.msg.text = '';		
		}

		// //- requisita msgs mandadas para a sala enquanto usuario estava offline
		// socket.emit('request offline data',disciplina_id,function(data){
		// 	console.log(data);
		// }); 

		// //- confirma recebimento das msg da sala e recebe callback com json{success}
		// socket.emit('receive offline data',disciplina_id,user_id,function(data){
		// 	console.log(data);
		// }) 
		
		
		//O cliente irá receber os eventos :


		//recebe o username do usuario q de desconectou
		socket.on('disconnect from room',function(username){
			$scope.mensagens.push("O usuario <"+username+"> desconectou.");
		}); 
		
		//recebe o username do usuario q se conectou e a nome da sala
		socket.on('user joined room',function(username,siglaDisciplina){
			$scope.mensagens.push("O usuario <"+username+"> está online.");
		});
		
		//recebe a mensagem , o usuario que enviou e a sala pra qual foi enviada
		socket.on('messaging',function(msg,user,siglaDisciplina){
				console.log("Mensagem: "+msg+" : "+user+" : "+siglaDisciplina);
				$scope.mensagens.push({
					"text":msg,
					"from":user,
					"disciplina":siglaDisciplina
				});
				$scope.$digest();
		
		});

		var conectUserChat = function(){

			
			var username = localStorageService.get('username');

			socket.emit('has username',function(data){
				console.log("dado : "+data);
				var hasUsername = data;
				console.log(hasUsername);


				if(hasUsername)
				console.log("Você já está conectado no chat !");
				else{
					//- associa o socket ao username e recebe uma callback com um json {sucess:boolean,msg:string,disciplinas:array}
					socket.emit('connect user',username,function(data){
						
						console.log(data);
					});
				}
			});

		}

		conectUserChat();
	

	
	}]);

} ());