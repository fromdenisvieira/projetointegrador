(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('PeriodosCtrl', ['$scope', '$rootScope','$location','$http', function($scope, $rootScope,$location,$http) {
		
		
		$scope.goDisciplina = function(idPeriodo){
			$location.path('disciplinas/'+idPeriodo);
		}

		$scope.$back = function() { 
		    window.history.back();
		};

	    $scope.carregaJSON = function() {
            $http.get('views/periodos/periodos.json').then(function(resultado){
                $scope.periodos = resultado.data;
            });
        }

        $scope.carregaJSON();

		/*
		DisciplinasFactory.listar().then(function(data){
			$scope.disciplinas = data;
			
			$scope.total = data.length;

		},
		function(){
			alert('erro');
		}); */

	}]);

} ());