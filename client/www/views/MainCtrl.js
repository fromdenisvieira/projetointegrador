(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('MainCtrl', ['$scope', '$rootScope','$location','$route', '$routeParams','localStorageService', function($scope, $rootScope, $location,$route,$routeParams,localStorageService) {
		
		$scope.$route = $route;
    	$scope.$location = $location;
    	$scope.$routeParams = $routeParams;
    
    	$scope.pageTitle = '';
    	
    	var username = localStorageService.get('username');
    	$scope.username = localStorageService.get('username');
    	//$scope.idPeriodo = $route.current.params.idPeriodo; 

    	$scope.$btnBack = function(){
    		
    		 window.history.back();

    	}

    	$scope.$back = function() { 
		    window.history.back();
		};
		

    	// Storage Dia de hoje
    	var dias_semana = new Array("Domingo", "Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira",	        "Sexta-feira", "Sábado");
		var data = new Date();
		var hoje = data.getDay();

		$scope.diaDeHoje = hoje+1;

    	localStorageService.set('diaDeHoje',$scope.diaDeHoje);
    	localStorageService.set('diaDaSemana',dias_semana[$scope.diaDeHoje-1]);

    	
    	$scope.goToChat = function(){
				$location.path('chatsList/'+username);
			}

		$scope.goToConfig = function(){
			$location.path('configuracoes');
		}

		$scope.goToDashboard = function(){
			$location.path('dashboard');
		}

		
		$scope.goToHorario = function(){
			$location.path('horario');
		} 
			
		// $scope.verifTitle = function(){

		// 	switch($location.path()){
				
		// 		case '/':
		// 			$scope.pageTitle = 'S.I CHECK';
		// 			break;	

		// 		case '/dashboard':
		// 			$scope.pageTitle = 'Dashboard';
		// 			break;

		// 		case '/periodos':
		// 			$scope.pageTitle = 'Periodos';
		// 			break;			

		// 		case '/configuracoes':
		// 			$scope.pageTitle = 'Configurações';
		// 			break;						

		// 		case '/cadastro':
		// 			$scope.pageTitle = 'Cadastro';
		// 			break;				

		// 		case '/horario':
		// 			$scope.pageTitle = 'Horário das Aulas';
		// 			break;					
		// 	}		
		
		// }
	}]);

} ());