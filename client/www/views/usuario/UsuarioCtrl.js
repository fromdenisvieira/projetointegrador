(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('UsuarioCtrl', ['$scope', 'UsuarioFactory', '$rootScope','$location','$route', function($scope, UsuarioFactory, $rootScope,$location,$route) {
		

		$scope.$back = function() { 
		    window.history.back();
		};
		

		$scope.salvar = function(usuario) {
			 $scope.falhou = false;
	    	UsuarioFactory.salvar(usuario).then(function(){

			      //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
			      //$scope.setCurrentUser(user);
		    }, function () {
		        $scope.falhou = true;
		    });
	    	 
		}


	}]);

} ());