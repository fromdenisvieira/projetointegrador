(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('ChatsListFactory', ['$http', '$location','$q','$route','localStorageService', function($http, $location,$q,$route,localStorageService) {

		var listar = function(){

			var retorno = $q.defer();

			var username = localStorageService.get('username');

			$http.get('http://projetoifal.herokuapp.com/api/users/'+username+'/disciplinas').success(function(data) {
				//console.log(data);
				retorno.resolve(data);
			})
			.error(function() {

				console.log("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;

		}
		
		
        var entrar = function (disciplina) {

			

        }
        
        var excluir = function (idDisciplina){

			

		}
    
        function atualizar (disciplina) {
        	
        	
        	
		}

		return {
			
			listar: listar,
			entrar: entrar,
			excluir: excluir,
			atualizar: atualizar
		};

	}]);


}());