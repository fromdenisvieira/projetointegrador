(function() {

	'use strict';

	angular.module('projetointegrador')
	.controller('HorarioCtrl', ['$scope','HorarioFactory','$rootScope','$location','$route', '$routeParams', function($scope,HorarioFactory, $rootScope, $location,$route,$routeParams) {
		
		$scope.$route = $route;
    	$scope.$location = $location;
    	$scope.$routeParams = $routeParams;

    	HorarioFactory.listar().then(function(data){
			
			$scope.disciplinas = data;
			
			$scope.total = data.length;

		},
		function(){
			alert('erro');
		});

		$scope.checkDia = function(disciplina,diaVerif,horaVerif){

			var cont = 0;

			angular.forEach(disciplina.horario, function(horario,index) {			

			   	if(horario.dia_da_semana == diaVerif && horario.hora == horaVerif)
	    			cont++    		
			})

			if(cont>0)			
				return true;
			else
				return false;
			
		}


    

	}]);

} ());