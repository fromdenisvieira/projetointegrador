/**
 * @ngdoc overview
 * @name unionApp
 * @description
 * # unionApp
 *
 * Main module of the application.
 */


(function () {
'use strict';

  angular.module('projetointegrador', [
    'ngRoute','ngAnimate','LocalStorageModule','angularModalService','btford.socket-io'
    ])
    .config(function($httpProvider) {
      //$http.defaults.headers.common.Authorization = 'Basic YmVlcDpib29w'
      $httpProvider.defaults.headers.delete = { 'Content-Type' : 'application/json' };
    })
    .config(['$routeProvider', function($routeProvider){
      $routeProvider
        .when('/', {
          templateUrl: 'views/login/login.html',
          controller: 'LoginCtrl'
        })
        .when('/dashboard', {
          templateUrl: 'views/dashboard/dashboard.html',
          controller: 'DashboardCtrl'
        })
        .when('/disciplinas/:idPeriodo', {
          templateUrl: 'views/disciplinas/disciplinas.html',
          controller: 'DisciplinasCtrl'
        })
        .when('/periodos', {
          templateUrl: 'views/periodos/periodos.html',
          controller: 'PeriodosCtrl'
        })
        .when('/ambientes', {
          templateUrl: 'views/ambientes/ambientes.html',
          controller: 'AmbientesCtrl'
        })
        .when('/configuracoes', {
          templateUrl: 'views/configuracoes/configuracoes.html',
          controller: 'ConfiguracoesCtrl'
        })
        .when('/chat/:sigla', {
          templateUrl: 'views/chat/chat.html',
          controller: 'ChatCtrl'
        })
        .when('/chatsList/:username', {
          templateUrl: 'views/chatsList/chatsList.html',
          controller: 'ChatsListCtrl'
        })
        .when('/cadastro', {
          templateUrl: 'views/usuario/cadUsuario.html',
          controller: 'UsuarioCtrl'
        })
        .when('/recuperarsenha', {
          templateUrl: 'views/recuperarsenha/recuperarsenha.html',
          controller: 'RecuperarSenhaCtrl'
        })
        .when('/horario', {
          templateUrl: 'views/horario/horario.html',
          controller: 'HorarioCtrl'
        })
        .when('/perfil', {
          templateUrl: 'views/perfil/perfil.html',
          controller: 'PerfilCtrl'
        })
        .otherwise({
          redirectTo: '/'
        });
    }])
    .run(['LoginFactory','AuthService','$rootScope','$location','ConfiguracoesFactory', function(LoginFactory,AuthService,$rootScope,$location,ConfiguracoesFactory){
        

        
        // LoginFactory.requestUser().then(function(){
        //     AuthService.makeReady();
        // });

        // $rootScope.$on('$locationChangeStart',function($scope,next,current){

        //   // if($location.path === '/') return;

        //   // if(! AuthService.isReady()){
        //   //   $location.path('/');
        //   // }

        //   // ConfiguracoesFactory.verifTitle().then(function(data){
        //   //   $rootScope.pageTitle = data;

        //   //   console.log($scope.pageTitle);
        //   // d  console.log('rodou teste'+data);
        //   //   $scope.$diggest();
        //   //   return $rootScope.pageTitle;  
        //   // },
        //   // function(){
        //   //   alert('erro');
        //   // }); 


        // })

      
    }]);


}());