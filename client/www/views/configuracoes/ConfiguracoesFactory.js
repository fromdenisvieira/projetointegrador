(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('ConfiguracoesFactory', ['$http', '$location','$q', function($http, $location,$q) {

		var listar = function(sequencial){


		}
		
		
        var salvar = function (disciplina) {

			

        }
        
        var excluir = function (idDisciplina){

			

		}
    
        var atualizar = function (disciplina) {
        	
        	
        	
		}

		var verifTitle = function () {

			var retorno = $q.defer();
        	
        	var pageTitle = '';
        			
			switch($location.path()){
				
				case '/':
					pageTitle = 'S.I CHECK';
					break;	

				case '/dashboard':
					pageTitle = 'Dashboard';
					break;

				case '/periodos':
					pageTitle = 'Periodos';
					break;			

				case '/configuracoes':
					pageTitle = 'Configurações';
					break;						

				case '/cadastro':
					pageTitle = 'Cadastro';
					break;				

				case '/horario':
					pageTitle = 'Horário das Aulas';
					break;					
			}		

			retorno.resolve(pageTitle);

			return retorno.promise;
		        	
		}



		return {
			
			listar: listar,
			salvar: salvar,
			excluir: excluir,
			atualizar: atualizar,
			verifTitle: verifTitle
		};

	}]);


}());