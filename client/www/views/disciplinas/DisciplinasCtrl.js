(function() {

	'use strict';

	angular.module('projetointegrador')
	.filter('capitalize', function() {
		return function(input, scope) {
		    return input.substring(0,1).toUpperCase()+input.substring(1);
		}
	})
	.controller('DisciplinasCtrl', ['$scope', 'DisciplinasFactory', '$rootScope','$location','$route','localStorageService', function($scope, DisciplinasFactory, $rootScope,$location,$route,localStorageService) {
		

		$scope.$back = function() { 
		    window.history.back();
		};
		
		$scope.username = localStorageService.get('username');

		DisciplinasFactory.listar($route.current.params.idPeriodo).then(function(data){
			$scope.disciplinas = data;			
			$scope.total = data.length;
		},
		function(){
			alert('erro');
		}); 

		$scope.verifCheckbox = function(disciplina){

			var cont = 0;

			angular.forEach(disciplina.usuarios_cadastrados, function(usuarios_cadastrados,index) {			

			   	if(usuarios_cadastrados.username == $scope.username)
	    			cont++    		
			})

			if(cont>0)			
				return true;
			else
				return false;

		}
		
		
		$scope.checkDisciplina = false;

		$scope.stateChanged = function (checkDisciplina,sigla) {
		   
		   console.log("oia :"+checkDisciplina);

		   if(!checkDisciplina){ //If it is checked

				DisciplinasFactory.excluir(sigla);
				// .then(function(data){
					
				// },
				// function(){
				// 	alert('erro');
				// }); 

		       console.log('desmarcado : Excluir = '+sigla);
		       
		   }else{

		   		DisciplinasFactory.salvar(sigla).then(function(data){
					
				},
				function(){
					alert('erro');
				}); 
		   		console.log('marcado : Salvar'+sigla);
		   }
		}

        $scope.idPeriodo = $route.current.params.idPeriodo; 

	}]);

} ());