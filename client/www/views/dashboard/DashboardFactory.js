(function() {

	'use strict';

	angular.module('projetointegrador')
	.factory('DashboardFactory', ['$http', '$location','$q','$route','localStorageService','$window', function($http, $location,$q,$route,localStorageService,$window) {

		var listarCheckins = function(){

			var retorno = $q.defer();
			

			$http.get('http://projetoifal.herokuapp.com/api/checkins').success(function(data) {
				//console.log(data);
				retorno.resolve(data);
			})
			.error(function() {

				console.log("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;

		}
		
		
        var listarAmbientes = function (ambientes) {

			var retorno = $q.defer();
			

			$http.get('http://projetoifal.herokuapp.com/api/ambientes').success(function(data) {
				//console.log(data);
				retorno.resolve(data);
			})
			.error(function() {

				console.log("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;

        }

        var doCheckin = function(ambiente,sigla){

        	var retorno = $q.defer();
        	
        	var username = localStorageService.get('username');

        	var nomedoambiente = '';

        	console.log("nome:"+ambiente.nome);
        	console.log("outronome:"+ambiente.outronome);

        	if(ambiente.nome){
		    	
		    	nomedoambiente = ambiente.nome;
        	}
		    else{
		    	nomedoambiente = ambiente.outronome;
		    }

		    console.log("ambiente: "+nomedoambiente);

        	var checkinToJson = function (){

	    		return angular.toJson({					
				    "sigla":sigla,
					"username":username,
					"ambiente":nomedoambiente

				})
			};

			console.log(checkinToJson());

			

			$http.post('http://projetoifal.herokuapp.com/api/checkins', checkinToJson())
			.success(function(data) {

				console.log("Checkin Feito com Sucesso !");
				console.log(data)
				$window.location.reload();
				//retorno.resolve(result);
			})
			.error(function() {

				alert("Aconteceu algo ruim! Verifique sua conexão de internet");
			}); 


			return retorno.promise;
        }

		return {
			
			listarCheckins: listarCheckins,
			listarAmbientes: listarAmbientes,
			 doCheckin : doCheckin 
		};

	}]);


}());