(function() {

	'use strict';

	angular.module('projetointegrador')
	.filter('porDia', function() {


		return function(input, scope) {
		    return input.substring(0,1).toUpperCase()+input.substring(1);
		}
	})
	.controller('ModalController', [
	  '$scope', '$element', 'nome','sigla', 'close','DashboardFactory', 
	  function($scope, $element, nome,sigla, close,DashboardFactory) {

	   $scope.nome = nome;
	   $scope.sigla = sigla;

  		DashboardFactory.listarAmbientes().then(function(data){
			$scope.ambientes = data;					
			$scope.total = data.length;
		},
			function(){
				alert('erro');
		}); 


  		$scope.btnCheckIn = function(ambiente){

			DashboardFactory.doCheckin(ambiente,$scope.sigla).then(function(data){				 
			      console.log(data);
			       $element.modal('hide');
	    			
				    //  Now call close, returning control to the caller.
				    $scope.cancel();
		    }, function () {
		        $scope.falhou = true;
		    });

		}

	
	  
	  //  This close function doesn't need to use jQuery or bootstrap, because
	  //  the button has the 'data-dismiss' attribute.
	  $scope.close = function() {
	 	  close({
	      name: $scope.name,
	      age: $scope.age
	    }, 500); // close, but give 500ms for bootstrap to animate
	  };

	  //  This cancel function must use the bootstrap, 'modal' function because
	  //  the doesn't have the 'data-dismiss' attribute.
	  $scope.cancel = function() {

	    //  Manually hide the modal.
	    $element.modal('hide');
	    
	    //  Now call close, returning control to the caller.
	    close({
	      name: $scope.name,
	      age: $scope.age
	    }, 500); // close, but give 500ms for bootstrap to animate
	  };

	}])
	.controller('DashboardCtrl', ['$scope', '$rootScope','$location','LoginFactory','AuthService','ChatsListFactory','localStorageService','DashboardFactory','ModalService', function($scope, $rootScope, $location,LoginFactory,AuthService,ChatsListFactory,localStorageService,DashboardFactory,ModalService) {
		

		$scope.diaDeHoje = localStorageService.get('diaDeHoje');	
		$scope.diaDaSemana = localStorageService.get('diaDaSemana');	

		  $scope.showModal = function(disciplina) {

		    ModalService.showModal({
		      templateUrl: "views/dashboard/modal.html",
		      controller: "ModalController",
		      inputs: {
		      	nome: disciplina.nome,
		        sigla: disciplina.sigla
		        
		      }
		    }).then(function(modal) {

		      modal.element.modal();
		      modal.close.then(function(result) {
		        
		      });
		    });

		  };
		
		$scope.checkDia = function(disciplina){

			var cont = 0;
			
			//console.log(disciplina.nome);

			angular.forEach(disciplina.horario, function(horario,index) {
				
			   //console.log("DIA DISCIPLINA : "+horario.dia_da_semana+" DIA DE HOJE : "+$scope.diaDeHoje);
			   
			   if(horario.dia_da_semana == $scope.diaDeHoje){
			   	 cont++;
			   }
			   	
			})

			if(cont>0)
				return true;
			else
				return false;
		}

		$scope.checkinFilterDiaDeHoje = function(checkin){

			var cont = 0;

			// console.log("dia : "+checkin.time+" Hoje : "+Date());
			
			
			// if(checkin.time == Date())
			// 	console.log("igual");
			// else
			// 	console.log("false")

			// console.log("CHECK "+checkin.time);
			// console.log("TS : "+Date());
			
			//console.log(disciplina.nome);

		
		}


		ChatsListFactory.listar().then(function(data){
			
			$scope.disciplinas = data;
			
			$scope.total = data.length;

		},
		function(){
			alert('erro');
		});

		DashboardFactory.listarCheckins().then(function(data){
			$scope.checkins = data;					
			$scope.total = data.length;
		},
		function(){
			alert('erro');
		}); 

		//console.log(LoginFactory.getUser());

		/*
		AuthService.registerListener(function(){
				// 
				$location.path('/');
		});
		*/

		/*
		AmbientesFactory.listar().then(function(data){
			$scope.ambientes = data;			
			$scope.total = data.length;
		},
		function(){
			alert('erro');
		}); 	

		$scope.salvarAmbiente = function(){

	    	   AmbientesFactory.salvar($scope.ambiente).then(function(data){
	    	   		    
	    	   },
	    	   function(){
	    	   	 alert('erro');
	    	   });	    	   
	    };
		*/

		$scope.goToHorario = function(){
			$location.path('horario');
		} 



	}]);

} ());